<?php

/*
 * Implements of the hook_menu()
 */
function og_notifications_menu() {
  $items['admin/config/messaging/subscriptions/organic_groups'] = array(
    'title' => 'Organic Groups',
    'description' => 'Organic Groups subscriptions',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('og_notifications_settings_form'),
    'access arguments' => array('administer site configuration'),
    'file' => 'og_notifications.admin.inc',
  );
  $items['group_label/autocomplete'] = array(
    'title' => 'Autocomplete group',
    'page callback' => 'og_notifications_autocomplete',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Check access to user account tab
 */
function og_notifications_account_access($account, $type) {
  return module_exists('notifications_account') && notifications_account_tab_access($account, $type);
}

/**
 * Implements hook_permission()
 */
function og_notifications_permission() {
  return array(
    'subscribe to a group' => array(
      'title' => t('Subscribe to content in a group.'),
      'description' => t('Subscribe to content posted in a group.'),
    ),
  );
}

/**
 * Implements of hook_notifications()
 */
function og_notifications_notifications($op) {
  switch ($op) {
    case 'subscription types':
      $types['group_content'] = array(
        'title' => t('All content in group'),
        'class' => 'Og_Notifications_Subscription',
        'field_types' => array('node:gid'),
        'object_types' => array('node', 'group'),
        'access' => array('subscribe to group'),
        'description' => t('Subscribe to all content submitted to a group.'),
      );
      return $types;

    case 'field types':
      $fields['node:gid'] = array(
        'title' => t('Group'),
        'class' => 'Og_Notifications_Field',
      );
      return $fields;
  }
}

/*
 * Get group ID by group label.
 */
function _notifications_get_gid_by_lable($name) {
  $result = db_select('og')
          ->fields('og', array('gid'))
          ->condition('label', $name)
          ->execute()
          ->fetchAll();
  if (!empty($result)) {
    return $result[0]->gid;
  }
  else {
    return;
  }
}

/**
 * Implements of hook notifications_subscription()
 */
function og_notifications_notifications_subscription($op, $subscription = NULL) {
  switch ($op) {
    case 'page objects':
      $objects = array();
      // Return objects on current page to which we can subscribe
      if (arg(0) == 'node' && is_numeric(arg(1)) && ($node = menu_get_object('node'))) {
        $objects[] = notifications_object('group', $node);
      }
      return $objects;
      break;
  }
}

/**
 * Implements of hook_notifications_object_group()
 */
function og_notifications_notifications_object_node($op, $node, $account = NULL) {
  switch ($op) {
    case 'subscription types':
      return array('group_content');
    case 'subscriptions':
      // Return available subscription options for this node
      $options = array();
      // Thread
      if (array_keys(og_get_all_group_content_entity())) {
        $options[] = notifications_subscription('group_content')
                ->add_field('node:gid', $node)
                ->set_group($node)
                ->set_name(t('All posts in this group.'));
      }
      return $options;
      break;
  }
}

/**
 * Menu callback for the autocomplete results.
 */
function og_notifications_autocomplete($field_name, $string = '') {
  $matches = array();
  $groups = og_field_audience_potential_groups($string, 'contains', array(), 10);
  $allowed_groups = variable_get('og_notifications', array());
  foreach ($groups as $gid => $label) {
    // Add a class wrapper for a few required CSS overrides.
    if (in_array($gid, $allowed_groups)) {
      $matches[$label] = '<div class="group-autocomplete">' . $label . '</div>';
    }
  }
  drupal_json_output($matches);
}
